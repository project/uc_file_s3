<?php

/**
 * @file
 * File menu items.
 */

/**
 * The number of bogus requests one IP address can make before being banned.
 */
define('UC_FILE_S3_REQUEST_LIMIT', 50);

/**
 * Download file chunk.
 */
define('UC_FILE_S3_BYTE_SIZE', 8192);

/**
 * Download statuses.
 */
define('UC_FILE_S3_ERROR_OK'                     , 0);
define('UC_FILE_S3_ERROR_NOT_A_FILE'             , 1);
define('UC_FILE_S3_ERROR_TOO_MANY_BOGUS_REQUESTS', 2);
define('UC_FILE_S3_ERROR_INVALID_DOWNLOAD'       , 3);
define('UC_FILE_S3_ERROR_TOO_MANY_LOCATIONS'     , 4);
define('UC_FILE_S3_ERROR_TOO_MANY_DOWNLOADS'     , 5);
define('UC_S3_FILE_ERROR_EXPIRED'                , 6);
define('UC_FILE_S3_ERROR_HOOK_ERROR'             , 7);

/**
 * Table builder for user downloads
 */
function uc_file_s3_user_downloads($account) {

  drupal_set_title(t('S3 File downloads'));

  // Create a header and the pager it belongs to.
  $header = array(
    array('data' => t('Purchased'  ), 'field' => 'u.granted', 'sort' => 'asc'),
    array('data' => t('Filename'   ), 'field' => 'f.filename'),
    array('data' => t('Description'), 'field' => 'p.description'),
    array('data' => t('Downloads'  ), 'field' => 'u.accessed'),
    array('data' => t('Addresses'  )),
  );

  $files = pager_query(
    "SELECT u.granted, f.filename, u.accessed, u.addresses, p.description, u.file_key, f.fid, u.download_limit, u.address_limit, u.expiration FROM {uc_file_s3_users} as u ".
    "LEFT JOIN {uc_files_s3} as f ON u.fid = f.fid ".
    "LEFT JOIN {uc_file_s3_products} as p ON p.pfid = u.pfid WHERE uid = %d".
    tablesort_sql($header),
    UC_FILE_S3_PAGER_SIZE,
    0,
    "SELECT COUNT(*) FROM {uc_file_s3_users} WHERE uid = %d",
    $account->uid
  );

  $rows = array();
  while ($file = db_fetch_object($files)) {

    $row = count($rows);
    $download_limit = $file->download_limit;

    // Set the JS behavior when this link gets clicked.
    $onclick = array(
      'attributes' => array(
        'onclick' => 'uc_file_s3_update_download('. $row .', '. $file->accessed .', '. ((empty($download_limit)) ? -1 : $download_limit) .');', 'id' => 'link-'. $row
      ),
    );

    // Expiration set to 'never'
    if ($file->expiration == FALSE) {
      $file_link = l(basename($file->filename), 'download-s3/'. $file->fid .'/'. $file->file_key, $onclick);
    }

    // Expired.
    elseif (time() > $file->expiration) {
      $file_link = basename($file->filename);
    }

    // Able to be downloaded.
    else {
      $file_link = l(basename($file->filename), 'download-s3/'. $file->fid .'/'. $file->file_key, $onclick) .' ('. t('expires on @date', array('@date' => format_date($file->expiration, 'custom', variable_get('uc_date_format_default', 'm/d/Y')))) .')';
    }

    $rows[] = array(
      array('data' => format_date($file->granted, 'custom', variable_get('uc_date_format_default', 'm/d/Y')), 'class' => 'date-row', 'id' => 'date-'. $row),
      array('data' => $file_link, 'class' => 'filename-row', 'id' => 'filename-'. $row),
      array('data' => $file->description, 'class' => 'description-row', 'id' => 'description-'. $row),
      array('data' => $file->accessed .'/'. ($file->download_limit ? $file->download_limit : t('Unlimited')), 'class' => 'download-row', 'id' => 'download-'. $row),
      array('data' => count(unserialize($file->addresses)) .'/'. ($file->address_limit ? $file->address_limit : t('Unlimited')), 'class' => 'addresses-row', 'id' => 'addresses-'. $row),
    );
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No downloads found'), 'colspan' => 5));
  }

  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, UC_FILE_S3_PAGER_SIZE, 0);
  $output .= '<div class="form-item"><p class="description">'.
  t('Once your download is finished, you must refresh the page to download again. (Provided you have permission)') .
  '<br />'. t('Downloads will not be counted until the file is finished transferring, even though the number may increment when you click.') .
  '<br /><b>'. t('Do not use any download acceleration feature to download the file, or you may lock yourself out of the download.') .'</b>'.
  '</p></div>';
  return $output;
}

/**
 * Handle file downloading and error states.
 *
 * @see _uc_file_s3_download_validate()
 *
 * @param $fid
 *   The fid of the file specified to download.
 * @param $key
 *   The hash key of a user's download
 */
function _uc_file_s3_download($fid, $key) {

  global $user;

  // Error messages for various failed download states.
  $admin_message = t('Please contact the site administrator if this message has been received in error.');
  $error_messages = array(
    UC_FILE_S3_ERROR_NOT_A_FILE              => t('The file you requested does not exist. '),
    UC_FILE_S3_ERROR_TOO_MANY_BOGUS_REQUESTS => t('You have attempted to download an incorrect file URL too many times. '),
    UC_FILE_S3_ERROR_INVALID_DOWNLOAD        => t("The following URL is not a valid download link. "),
    UC_FILE_S3_ERROR_TOO_MANY_LOCATIONS      => t('You have downloaded this file from too many different locations. '),
    UC_FILE_S3_ERROR_TOO_MANY_DOWNLOADS      => t('You have reached the download limit for this file. '),
    UC_S3_FILE_ERROR_EXPIRED                 => t("This file download has expired. "),
    UC_FILE_S3_ERROR_HOOK_ERROR              => t("A hook denied your access to this file. "),
  );

  //fail if S3 connection parameters are not set.
  if (!uc_file_s3_is_connected()) {
    drupal_set_message(t('This site is not properly configured to connect to Amazon S3.  Please contact your site administrator.'), 'error');
  }

  $ip = ip_address();
  $file_download = uc_file_s3_get_by_key($key);
  
  // If it's ok, we push the file to the user.
  $status = _uc_file_s3_download_validate($file_download, $user, $ip);
  if ($status == UC_FILE_S3_ERROR_OK) {
    _uc_file_s3_download_transfer($file_download, $ip);
  }

  // Some error state came back, so report it.
  else {
    drupal_set_message($error_messages[$status] . $admin_message, 'error');
  }

  // Kick 'em to the curb. >:)
  _uc_file_s3_download_redirect($user->uid);
}

/**
 * Perform first-pass authorization. Call authorization hooks afterwards.
 *
 * Called when a user requests a file download, function checks download
 * limits then checks for any implementation of hook_download_authorize.
 * Passing that, the function _uc_file_s3_download_transfer is called.
 *
 * @param $file_download
 *   The file download object, loaded by uc_file_s3_get_by_key()
 * @param $user
 *   The user object doing the downloading.
 * @param $ip
 *   The ip address of the user downloading the file.
 */
function _uc_file_s3_download_validate($file_download, &$user, $ip) {

  $request_cache = cache_get('uc_file_s3_'. $ip);
  $requests = ($request_cache) ? $request_cache->data + 1 : 1;

  $message_user = ($user->uid) ? t('The user %username ', array('%username' => $user->name)) : t('The IP address %ip ', array('%ip' => $ip));

  if ($requests > UC_FILE_S3_REQUEST_LIMIT) {
    return UC_FILE_S3_ERROR_TOO_MANY_BOGUS_REQUESTS;
  }

  // Must be a valid file.
  if (!$file_download || !uc_file_s3_file_exists($file_download)) {
    cache_set('uc_file_s3_'. $ip, $requests, 'cache', time() + 86400);
    if ($requests == UC_FILE_S3_REQUEST_LIMIT) {
      watchdog('uc_file_s3', '%username has been temporarily banned from file downloads.', array('%username' => $message_user), WATCHDOG_WARNING);
    }

    return UC_FILE_S3_ERROR_INVALID_DOWNLOAD;
  }

  $addresses = $file_download->addresses;

  // Check the number of locations.
  if (!empty($file_download->address_limit) && !in_array($ip, $addresses) && count($addresses) >= $file_download->address_limit) {
    watchdog('uc_file_s3', '%username has been denied a file download by downloading it from too many IP addresses.', array('%username' => $message_user), WATCHDOG_WARNING);

    return UC_FILE_S3_ERROR_TOO_MANY_LOCATIONS;
  }

  // Check the downloads so far.
  if (!empty($file_download->download_limit) && $file_download->accessed >= $file_download->download_limit) {
    watchdog('uc_file_s3', '%username has been denied a file download by downloading it too many times.', array('%username' => $message_user), WATCHDOG_WARNING);

    return UC_FILE_S3_ERROR_TOO_MANY_DOWNLOADS;
  }

  // Check if it's expired.
  if ($file_download->expiration && time() >= $file_download->expiration) {
    watchdog('uc_file_s3', '%username has been denied an expired file download.', array('%username' => $message_user), WATCHDOG_WARNING);

    return UC_S3_FILE_ERROR_EXPIRED;
  }

  //Check any if any hook_s3_download_authorize calls deny the download
  foreach (module_implements('s3_download_authorize') as $module) {
    $name = $module .'_s3_download_authorize';
    $result = $name($user, $file_download);
    if (!$result) {
      return UC_FILE_S3_ERROR_HOOK_ERROR;
    }
  }

  // Everything's ok!
  watchdog('uc_file_s3', '%username has started download of the S3 file %filename.', array('%username' => $message_user, '%filename' => $file_download->filename), WATCHDOG_NOTICE);
}

/**
 * Update the uc_file_s3_users table and send them to S3 for the file download.
 *
 * @param $file_user
 *   The file_user object from the uc_file_s3_users
 * @param $ip
 *   The string containing the ip address the download is going to
 * @param $fid
 *   The file id of the file to transfer
 */
function _uc_file_s3_download_transfer($file_user, $ip) {

  // TODO: implement a hook for altering S3 download transfers.
  // Check if any hook_s3_file_transfer_alter calls alter the download.
  /*foreach (module_implements('s3_file_transfer_alter') as $module) {
    $name = $module .'_s3_file_transfer_alter';
    $file_user->full_path = $name($file_user, $ip, $fid, $file_user->full_path);
  }*/

  $aws_bucket = $file_user->bucketname;
  $aws_object = $file_user->filename;
  
  $aws_key = variable_get('aws_access_key', NULL);
  $aws_secret = variable_get('aws_secret_key', NULL);
  
  //connect to S3 with a socket.
  $dt = gmdate('r'); // GMT based timestamp 

  // preparing string to sign
  $string2sign = "GET\n\n\n{$dt}\n/{$aws_bucket}/" . urlencode($aws_object);

  // preparing HTTP query 
  $query = "GET /" . urlencode($aws_object) . " HTTP/1.1\n".
  "Host: " . $aws_bucket . ".s3.amazonaws.com\n".
  "Connection: close\n".
  "Date: {$dt}\n".
  "Authorization: AWS {$aws_key}:" . _uc_file_s3_amazon_hmac($string2sign, $aws_secret) . "\n\n";  
  
  $fp = fsockopen($aws_bucket . ".s3.amazonaws.com", 80, $errno, $errstr, 30);
  
  if (!$fp) die("$errstr ($errno)\n");

  fwrite($fp, $query);

  $s3 = amazon_s3_get_instance();
  $s3_file_info = $s3->getObjectInfo($file_user->bucketname, $file_user->filename);

  // Standard headers, including content-range and length
  drupal_set_header('Pragma: public');
  drupal_set_header('Cache-Control: cache, must-revalidate');
  drupal_set_header('Accept-Ranges: bytes');
  drupal_set_header('Content-Range: bytes '. '0' .'-'. $s3_file_info['size'] .'/'. $s3_file_info['size']);
  drupal_set_header('Content-Type: '. $s3_file_info['type']);
  drupal_set_header('Content-Disposition: attachment; filename="'. $file_user->filename .'"');
  drupal_set_header('Content-Length: '. $s3_file_info['size']);

  // Last-modified is required for content served dynamically
  drupal_set_header('Last-modified: '. format_date($s3_file_info['time'], 'large'));

  // Etag header is required for Firefox3 and other managers
  drupal_set_header('ETag: '. $s3_file_info['hash']);

  $check_header = TRUE;
  // Start buffered download
  while (!feof($fp)) {
    $chunk = fread($fp, UC_FILE_S3_BYTE_SIZE); // reading response
    
    //find the headers and get rid of them, we don't want to output them with the file data.
    if ($check_header) {
      //search for the \r\n\r\n string that marks the header boundary
      $header_end = strpos($chunk, "\r\n\r\n"); // this is HTTP header boundary
      if ($header_end !== FALSE) {
        $check_header = FALSE;
        
        //ditch the headers, with an extra 4 bytes for the \r\n\r\n boundary
        $chunk = substr($chunk, $header_end+4);
      }
    }
    
    print($chunk);
  }

  //close the socket
  fclose($fp);
  
  //log the file to the downloads table.
  _uc_file_s3_log_download($file_user, $ip);
}

// hmac-sha1 code START
// hmac-sha1 function:  assuming key is global $aws_secret 40 bytes long
// read more at http://en.wikipedia.org/wiki/HMAC
// warning: key($aws_secret) is padded to 64 bytes with 0x0 after first function call 

/**
 * Function to sign requests for Amazon S3.
 * 
 * @param $string_to_sign
 *   The string to sign
 * @param @aws_secret
 *   The AWS secret key to use for signing.
 */
function _uc_file_s3_amazon_hmac($string_to_sign, $aws_secret) {
    // helper function binsha1 for amazon_hmac (returns binary value of sha1 hash)
    if (!function_exists('binsha1')) { 
        if (version_compare(phpversion(), "5.0.0", ">=")) { 
            function binsha1($d) { return sha1($d, TRUE); }
        }
        else {
            function binsha1($d) { return pack('H*', sha1($d)); }
        }
    }

    if (strlen($aws_secret) == 40)
        $aws_secret = $aws_secret . str_repeat(chr(0), 24);

    $ipad = str_repeat(chr(0x36), 64);
    $opad = str_repeat(chr(0x5c), 64);

    $hmac = binsha1(($aws_secret ^ $opad) . binsha1(($aws_secret^$ipad) . $string_to_sign));
    return base64_encode($hmac);
}

/**
 * Process and log a file download.
 */
function _uc_file_s3_log_download($file_user, $ip) {

  // Add the address if it doesn't exist.
  $addresses = $file_user->addresses;
  if (!in_array($ip, $addresses)) {
    $addresses[] = $ip;
  }
  $file_user->addresses = $addresses;

  // Accessed again.
  $file_user->accessed++;

  // Calculate hash
  $file_user->file_key = drupal_get_token(serialize($file_user));

  drupal_write_record('uc_file_s3_users', $file_user, 'fuid');
}

/**
 * Send 'em packin.
 */
function _uc_file_s3_download_redirect($uid = NULL) {

  // Shoo away anonymous users.
  if ($uid == 0) {
    drupal_access_denied();
  }
  // Redirect users back to their file page.
  else {
    drupal_goto('user/'. $uid .'/purchased-files-s3');
  }
}

