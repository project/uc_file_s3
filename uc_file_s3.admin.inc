<?php

/**
 * @file
 * File administration menu items.
 */

/**
 * Form step values.
 */
define('UC_FILE_S3_FORM_FILES' , NULL);
define('UC_FILE_S3_FORM_ACTION', 1);

/**
 * Callback to show buckets available on an S3 account.
 */
function uc_file_s3_view_buckets() {
  $output = '';
  
  //fail if S3 account not connected.
  if (!uc_file_s3_is_connected()) {
    $output .= '<p>'. t('Your Amazon S3 connection parameters are not correct or are not set up.  You must <a href="!s3_settings_url">setup your Amazon S3 connection parameters</a> to use Amazon S3 File Downloads.', array('!s3_settings_url' => url('admin/settings/amazon-s3'))) . '</p>';
    return $output;
  }
  
  $bucket_names = amazon_s3_get_buckets();
  $buckets = array();
  foreach ($bucket_names as $bucket_name) {
    $buckets[] = array(
      'name' => $bucket_name,
      'view_files' => l('View Files', "admin/store/products/files-s3/$bucket_name"),
    );
  }
  
  $table = theme('table', array('Name', 'View Files'), $buckets);

  //$bucket_create_form = drupal_get_form('amazon_s3_bucket_create_form');
  
  //return $bucket_create_form . $table;

  $output .= '<p>'. t("Below is a list of the buckets associated with your Amazon S3 account.  To view the Amazon S3 file downloads in any bucket, click on the 'View Files' link in the bucket's row.") . '</p>';

  $output .= $table;

  return $output;
}

/**
 * Form builder for file products admin.
 *
 * @ingroup forms
 * @see
 *   uc_file_s3_admin_files_form_show_files()
 *   uc_file_s3_admin_files_form_action()
 */
function uc_file_s3_admin_files_form($form_state, $bucket) {

  switch ($form_state['storage']['step']) {

    case UC_FILE_S3_FORM_FILES:

      // Refresh our file list before display.
      uc_file_s3_refresh();

      return array(
        '#theme'  => 'uc_file_s3_admin_files_form_show',
        '#validate' => array('uc_file_s3_admin_files_form_show_validate'),
        '#submit' => array('uc_file_s3_admin_files_form_show_submit'),
      ) + uc_file_s3_admin_files_form_show_files($form_state, $bucket);

    case UC_FILE_S3_FORM_ACTION:

      return array(
        '#validate' => array('uc_file_s3_admin_files_form_action_validate'),
        '#submit'   => array('uc_file_s3_admin_files_form_action_submit'),
      ) + uc_file_s3_admin_files_form_action($form_state);

  }
}

/**
 * Display all files that may be purchased and downloaded for administration.
 *
 * @ingroup forms
 * @see
 *   uc_file_s3_admin_files_form()
 *   uc_file_s3_admin_files_form_show_validate()
 *   uc_file_s3_admin_files_form_show_submit()
 *   theme_uc_file_s3_admin_files_form_show()
 */
function uc_file_s3_admin_files_form_show_files($form_state, $bucket) {

  $bid = db_result(db_query("SELECT bid FROM {uc_file_s3_buckets} WHERE bucketname = '%s'", $bucket));//$form_state['values']['bucket']));

  $form['#tree'] = TRUE;

  $form['#header'] = array(
    array(),
    array('data' => t('File'), 'field' => 'f.filename', 'sort' => 'asc'),
    array('data' => t('Product'), 'field' => 'n.title'),
    array('data' => t('SKU'), 'field' => 'fp.model')
  );

  // Create pager.
  $query = pager_query(
    "SELECT n.nid, f.filename, n.title, fp.model, f.fid, pf.pfid FROM {uc_files_s3} as f ".
    "LEFT JOIN {uc_file_s3_products} as fp ON (f.fid = fp.fid) ".
    "LEFT JOIN {uc_product_features} as pf ON (fp.pfid = pf.pfid) ".
    "LEFT JOIN {node} as n ON (pf.nid = n.nid) ".
    "WHERE f.bid = $bid ".
    tablesort_sql($form['#header']),
    UC_FILE_S3_PAGER_SIZE,
    0,
    "SELECT COUNT(*) FROM {uc_files_s3} as f WHERE f.bid = $bid"
  );

  // Create checkboxes for each file.
  $form['file_select'] = array('#tree' => TRUE);
  while ($file = db_fetch_object($query)) {
    $form['file_select'][$file->fid]['check'] = array('#type' => 'checkbox');
    $form['file_select'][$file->fid]['filename'] = array('#value' => $file->filename);
    $form['file_select'][$file->fid]['title'] = array('#value' => $file->title);
    $form['file_select'][$file->fid]['nid'] = array('#value' => $file->nid);
    $form['file_select'][$file->fid]['model'] = array('#value' => $file->model);
  }

  // Implement a Checkall / Uncheck all deal.
  $check_all = l(t('Check all'), "admin/store/products/files-s3/$bucket", array('attributes' => array('id' => 'uc_file_s3_select_all'), 'fragment' => 'NULL'));
  $uncheck_all = l(t('Uncheck all'), "admin/store/products/files-s3/$bucket", array('attributes' => array('id' => 'uc_file_s3_select_none'), 'fragment' => 'NULL'));
  $form['uc_file_s3_select'] = array(
    '#type' => 'markup',
    '#value' => $check_all .' / '. $uncheck_all,
  );

  $form['uc_file_s3_action'] = array(
    '#type' => 'fieldset',
    '#title' => t('File options for %bucket', array('%bucket' => $bucket)),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // Set our default actions.
  $file_actions = array(
    'uc_file_s3_upload' => t('Upload file'),
    'uc_file_s3_delete' => t('Delete file(s)'),
  );

  // Check if any hook_s3_file_action('info', $args) are implemented
  foreach (module_implements('s3_file_action') as $module) {
    $name = $module .'_s3_file_action';
    $result = $name('info', NULL);
    if (is_array($result)) {
      foreach ($result as $key => $action) {
        if ($key != 'uc_file_s3_delete' && $key != 'uc_file_s3_upload') {
          $file_actions[$key] = $action;
        }
      }
    }
  }

  $form['uc_file_s3_action']['action'] = array(
    '#type' => 'select',
    '#title' => t('Action'),
    '#options' => $file_actions,
    '#prefix' => '<div class="s3-duration">',
    '#suffix' => '</div>',
  );

  $form['uc_file_s3_action']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Perform action'),
    '#prefix' => '<div class="s3-duration">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Ensure at least one file is selected when deleting.
 *
 * @see uc_file_s3_admin_files_form_show_files()
 */
function uc_file_s3_admin_files_form_show_validate($form, &$form_state) {

  switch ($form_state['values']['uc_file_s3_action']['action']) {

    case 'uc_file_s3_delete':
      $file_ids = array();
      if (is_array($form_state['values']['file_select'])) {
        foreach ($form_state['values']['file_select'] as $fid => $value) {
          if ($value) {
            $file_ids[] = $fid;
          }
        }
      }

      if (count($file_ids) == 0) {
        form_set_error('', t('You must select at least one file to delete.'));
      }

      break;
  }

}

/**
 * Move to the next step of the administration form.
 *
 * @see uc_file_s3_admin_files_form_show_files()
 */
function uc_file_s3_admin_files_form_show_submit($form, &$form_state) {
  // Increment the form step.
  $form_state['storage']['step'] = UC_FILE_S3_FORM_ACTION;
  $form_state['rebuild'] = TRUE;
}

/**
 * @ingroup themeable
 * @see uc_file_s3_admin_files_form_show_files()
 */
function theme_uc_file_s3_admin_files_form_show($form) {

  $output = '';

  //fail if Amazon S3 account not connected.
  if (!uc_file_s3_is_connected()) {
    $output .= '<p>' . t('Your Amazon S3 connection parameters are not correct or are not set up.  You must <a href="!s3_settings_url">setup your Amazon S3 connection parameters</a> to use Amazon S3 File Downloads.', array('!s3_settings_url' => url('admin/settings/amazon-s3'))) . '</p>';
    return $output;
  }

  $header = $form['#header'];

  // Format the file table.
  $rows = array();
  foreach ($form['file_select'] as $fid => $value) {

    // We only want numeric fid's
    if (!is_numeric($fid)) {
      continue;
    }

    $filename = drupal_render($form['file_select'][$fid]['filename']);

    $class = '';

    $rows[] = array(
      array('data' => drupal_render($form['file_select'][$fid]['check'])),
      array('data' => $filename, 'class' => $class),
      array('data' => l(drupal_render($form['file_select'][$fid]['title']), 'node/'. drupal_render($form['file_select'][$fid]['nid']))),
      array('data' => drupal_render($form['file_select'][$fid]['model']))
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No file downloads available.'), 'colspan' => 4));
  }

  // Render everything.
  $output .= '<p>'. t('Amazon S3 file downloads can be attached to any Ubercart product as a product feature by going to <a href="!view_products_url">view products</a>, choosing a product, choosing edit, and choosing the features tab. If you need to change your S3 connection parameters, click <a href="!s3_settings_url">here</a>. To view all buckets in your S3 account, click <a href="!s3_buckets">here</a>.', array('!view_products_url' => url('admin/store/products/view'), '!s3_settings_url' => url('admin/settings/amazon-s3'), '!s3_buckets' => url('admin/store/products/files-s3/'))) .  '</p>';

  $s3 = amazon_s3_get_instance();
  
  $output .= drupal_render($form['uc_file_s3_action']);
  $output .= drupal_render($form['uc_file_s3_select']);
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, UC_FILE_S3_PAGER_SIZE, 0);
  $output .= drupal_render($form);

  return $output;
}

/**
 * Perform file action (upload, delete, hooked in actions).
 *
 * @ingroup forms
 * @see
 *   uc_file_s3_admin_files_form()
 *   uc_file_s3_admin_files_form_action_validate()
 *   uc_file_s3_admin_files_form_action_submit()
 */
function uc_file_s3_admin_files_form_action($form_state) {
  $file_ids = array();
  foreach ((array)$form_state['values']['file_select'] as $fid => $value) {
    $value = $value['check'];
    if ($value) {
      $file_ids[] = $fid;
    }
  }

  $form['file_ids'] = array('#type' => 'value', '#value' => $file_ids);
  $form['action'] = array('#type' => 'value', '#value' => $form_state['values']['uc_file_s3_action']['action']);

  //$file_ids = _uc_file_s3_sort_names(_uc_file_s3_get_dir_file_ids($file_ids, FALSE));
  $file_ids = _uc_file_s3_sort_names($file_ids);

  switch ($form_state['values']['uc_file_s3_action']['action']) {

    case 'uc_file_s3_delete':
      $affected_list = _uc_file_s3_build_js_file_display($file_ids);

      foreach ($file_ids as $file_id) {

        // Gather a list of user-selected filenames.
        $file = uc_file_s3_get_by_id($file_id);
        $filename = $file->filename;
        $file_list[] = $filename;
      }

      // Base files the user selected.
      $form['selected_files'] = array(
        '#type' => 'markup',
        '#value' => theme_item_list($file_list, NULL, 'ul', array('class' => 'selected-file-name')),
      );

      $form = confirm_form(
        $form, t('Delete file(s)'),
        'admin/store/products/files-s3',
        t('Deleting a file will remove all its associated file downloads and product features.'),
        t('Delete affected files'), t('Cancel')
      );

      break;

    case 'uc_file_s3_upload': //Upload file

      // Calculate the max size of uploaded files, in bytes.
      $max_bytes = trim(ini_get('post_max_size'));
      switch (strtolower($max_bytes{strlen($max_bytes)-1})) {
        case 'g':
          $max_bytes *= 1024;
        case 'm':
          $max_bytes *= 1024;
        case 'k':
          $max_bytes *= 1024;
      }

      //grab the bucket list from S3 so we can verify that all the buckets are still there
      $s3_bucket_list = amazon_s3_get_buckets();

      //gather list of buckets
      $bucketoptions = array();
      $buckets = db_query("SELECT * FROM {uc_file_s3_buckets}");
      while ($bucket = db_fetch_object($buckets)) {
        if (in_array($bucket->bucketname, $s3_bucket_list))
          $bucketoptions[$bucket->bucketname] = $bucket->bucketname;
      }
      
      $form['upload_bucket'] = array(
        '#type' => 'select',
        '#title' => t('Bucket'),
        '#description' => t('The bucket to upload the file to.'),
        '#options' => $bucketoptions,
      );
      $form['upload'] = array(
        '#type' => 'file',
        '#title' => t('File'),
        '#description' => t('The maximum file size that can be uploaded is %size bytes. You will need to use a different method to upload the file to the bucket (e.g. S3Fox) if your file exceeds this size.<br />The module will automatically detect files you upload using an alternate method.', array('%size' => number_format($max_bytes))),
      );
      
      $form['acl'] = array(
        '#type' => 'select',
        '#title' => t('Access Control'),
        '#options' => amazon_s3_acl_options(),
        '#description' => t('Choose the access control level to associate with the uploaded file.  "Private" is recommended.'),
      );


      $form['#attributes']['class'] .= 'foo';
      $form = confirm_form(
        $form, t('Upload file'),
        'admin/store/products/files-s3',
        '',
        t('Upload file'), t('Cancel')
      );

      // Must add this after confirm_form, as it runs over $form['#attributes'].
      // Issue logged at d#319723
      $form['#attributes']['enctype'] = 'multipart/form-data';

      break;

    default:

      // This action isn't handled by us, so check if any hook_file_s3_action('form', $args) are implemented.
      foreach (module_implements('s3_file_action') as $module) {
        $name = $module .'_s3_file_action';
        $result = $name('form', array('action' => $form_state['values']['uc_file_s3_action']['action'], 'file_ids' => $file_ids));
        $form = (is_array($result)) ? array_merge($form, $result) : $form;
      }

      break;
  }

  return $form;
}

/**
 * @see uc_file_s3_admin_files_form_action()
 */
function uc_file_s3_admin_files_form_action_validate($form, &$form_state) {

  switch ($form_state['values']['action']) {

    case 'uc_file_s3_upload':

      // Upload the file and get its object.
      if ($temp_file = file_save_upload('upload', array())) {

        // Check if any hook_s3_file_action('upload_validate', $args) are implemented.
        foreach (module_implements('s3_file_action') as $module) {
          $name = $module .'_s3_file_action';
          $result = $name('upload_validate', array('file_object' => $temp_file, 'form_id' => $form_id, 'form_state' => $form_state));
        }

        // Save the uploaded file for later processing.
        $form_state['storage']['temp_file'] = $temp_file;
      }
      else {
        form_set_error('', t('An error occurred while uploading the file'));
      }

      break;

    default:

      // This action isn't handled by us, so check if any hook_s3_file_action('validate', $args) are implemented
      foreach (module_implements('s3_file_action') as $module) {
        $name = $module .'_s3_file_action';
        $result = $name('validate', array('form_id' => $form_id, 'form_state' => $form_state));
      }

      break;
  }
}

/**
 * @see uc_file_s3_admin_files_action()
 */
function uc_file_s3_admin_files_form_action_submit($form, &$form_state) {

  switch ($form_state['values']['action']) {

    case 'uc_file_s3_delete':

      // File deletion status.
      $status = TRUE;

      // Delete the selected file(s)
      $status = uc_file_s3_remove_by_id($form_state['values']['file_ids']) && $status;

      if ($status) {
        drupal_set_message(t('The selected file(s) have been deleted.'));
      }
      else {
        drupal_set_message(t('One or more files could not be deleted.'));
      }

      break;

    case 'uc_file_s3_upload':
      
      $bucket = $form_state['values']['upload_bucket'];
      
      //make sure the bucket exists
      $bucketlist = amazon_s3_get_buckets();
      if (in_array($bucket, $bucketlist)) {
        
        $s3 = amazon_s3_get_instance();
        
        // Retrieve our uploaded file.
        $file_object = $form_state['storage']['temp_file'];
          
        //grab the bucket name from the form.
        $bucket = $form_state['values']['upload_bucket'];

        $acl = $form_state['values']['acl'];
  
        //upload the file to S3.
        $success = $s3->putObjectFile($file_object->filepath, $bucket, $file_object->filename, $acl);

        // run hooks and housekeeping if the upload succeeded.
        if ($success) {

          // Check if any hook_s3_file_action('upload', $args) are implemented
          foreach (module_implements('s3_file_action') as $module) {
            $name = $module .'_s3_file_action';
            $result = $name('upload', array('file_object' => $file_object, 'form_id' => $form_id, 'form_state' => $form_state));
          }

          // Update the file list
          uc_file_s3_refresh();

          drupal_set_message(t('The file %file has been uploaded to %bucket', array('%file' => $file_object->filename, '%bucket' => $bucket)));
        }
        else {
          drupal_set_message(t('An error occurred while uploading the file to %bucket', array('%bucket' => $bucket)));
        }
      }
      else {
        drupal_set_message(t('Can not move file to %bucket', array('%bucket' => $bucket)));
      }

      break;

    default:

      // This action isn't handled by us, so check if any hook_s3_file_action('submit', $args) are implemented
      foreach (module_implements('s3_file_action') as $module) {
        $name = $module .'_s3_file_action';
        $result = $name('submit', array('form_id' => $form_id, 'form_state' => $form_state));
      }
      break;
  }

  // Return to the original form state.
  $form_state['rebuild'] = FALSE;
  drupal_goto('admin/store/products/files-s3');
}

/**
 * TODO: Replace with == operator?
 */
function _uc_file_s3_display_arrays_equivalent($recur, $no_recur) {

  // Different sizes.
  if (count($recur) != count($no_recur)) {
    return FALSE;
  }

  // Check the elements.
  for ($i = 0; $i < count($recur); $i++) {
    if ($recur[$i] != $no_recur[$i]) {
      return FALSE;
    }
  }

  return TRUE;
}

/**
 * Show all possible files in selectable list.
 */
function _uc_file_s3_build_js_file_display($file_ids) {

  // Gather the files if recursion is selected. Get 'em all ready to be punched into
  // the file list.
  $recursion_file_ids = _uc_file_s3_sort_names($file_ids);
  foreach ($recursion_file_ids as $file_id) {
    $file = uc_file_s3_get_by_id($file_id);
    $recursion[] = '<li>'. $file->filename .'</li>';
  }

  // Gather the files if recursion isn't selected. Get 'em all ready to be punched into
  // the file list.
  $no_recursion_file_ids = $file_ids;
  foreach ($no_recursion_file_ids as $file_id) {
    $file = uc_file_s3_get_by_id($file_id);
    $no_recursion[] = '<li>'. $file->filename .'</li>';
  }

  // We'll disable the recursion checkbox if they're equal.
  $equivalent = _uc_file_s3_display_arrays_equivalent($recursion_file_ids, $no_recursion_file_ids);

  // The list to show if the recursion checkbox is $key.
  $result[TRUE] = $equivalent ? FALSE : $recursion;
  $result[FALSE] = $no_recursion;

  // Set up some JS to dynamically update the list based on the
  // recursion checkbox state.
  drupal_add_js('uc_file_s3_list[false] = '. drupal_to_js('<li>'. implode('</li><li>', $no_recursion) .'</li>') .';', 'inline');
  drupal_add_js('uc_file_s3_list[true] = '. drupal_to_js('<li>'. implode('</li><li>', $recursion) .'</li>') .';', 'inline');

  return $result;
}
